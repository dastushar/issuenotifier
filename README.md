#Gitlab Issue Notifier

When an issue is created in a gitlab repository, there is no notification triggered, unless the issue is assigned to a project member.

The gitlab-issue-notifier notifies the user of the number of issues created at a frequency of every 30 minutes. 

Logs all the open issues and the name of the author.

Also prompts a notification giving the count of the issue created within last 30 minutes.

##Usage
```
go install https://gitlab.com/dastushar/issuenotifier

./issueNotifier `<projectId>` `<host>`
```

where `projectId` is the id of the project you want the issues notification, host is the host of the project.

`projectId` can be found on the project's page, below the name of the project 

Example:- `./issueNotifier 12 gitlab.com` 

Keep this running in a terminal and don't terminate if you want to check the issues every 30 minutes.

First run will list all the open issues.
