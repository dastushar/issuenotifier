package gitlab

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	gosxnotifier "github.com/deckarep/gosx-notifier"
)

type Issues struct {
	Description string                 `json:"description"`
	Title       string                 `json:"title"`
	CreatedAt   string                 `json:"created_at"`
	Author      map[string]interface{} `json:"author"`
	WebUrl      string                 `json:"web_url"`
}

func FetchIssues(host string, projectId string) {
	if host == "" {
		host = "gitlab.com"
	}

	// gitlab api to fetch all the open issues
	url := fmt.Sprintf("https://%s/api/v4/projects/%s/issues?state=opened", host, projectId)

	currTime := time.Time{}
	var res []Issues

	for {
		log.Println("checking for new issues since " + currTime.String())

		err := bindResponse(url, &res)
		if err != nil {
			log.Println(err)
			time.Sleep(30 * time.Minute)
			continue
		}

		processIssues(res, currTime)
		log.Print("----------------------------------------------------------------------------------------------")
		currTime = time.Now()
		time.Sleep(30 * time.Minute)
	}
}

// bindResponse binds the response to the target interface
func bindResponse(url string, target interface{}) error {
	client := http.Client{}

	r, err := client.Get(url)
	if err != nil {
		return err
	}

	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

// processIssues filters all the new issues created after the last time `processIssues` ran
// first run results logging all the open issues
func processIssues(issues []Issues, time time.Time) {
	count := 0
	for _, v := range issues {
		if v.CreatedAt > time.String() {
			log.Printf("%v :: %v :: %v", v.Title, v.Author["name"], v.WebUrl)
			count++
		}
	}

	if count > 0 {
		log.Println("No. of issues found: ", strconv.Itoa(count))
		note := gosxnotifier.NewNotification(strconv.Itoa(count) + " new issues found in the project")
		note.Title = fmt.Sprintf("Gitlab Issues")
		note.Sound = gosxnotifier.Submarine
		note.Push()
	}
}
