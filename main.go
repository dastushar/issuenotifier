package main

import (
	"fmt"
	"os"

	"gitlab.com/issuenotifier/platform/gitlab"
)

func main() {
	args := os.Args

	if len(args) < 2 {
		fmt.Println("provide projectId and host. \nUsage: `./main <projectId> <host>`\nHost is optional, default gitlab.com")
		return
	}

	projectId := args[1]

	var host string

	if len(args) >= 3 {
		host = args[2]
	}

	gitlab.FetchIssues(host, projectId)
}
